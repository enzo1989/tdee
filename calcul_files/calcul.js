var day = 0, bmr = 0, activebmr = 0, heat = 0;
var num1 =0, num2 = 0, num3 = 0;
var weekSpeed = 0, fatBenchmark = 0, calorie = 0, proteinBenchmark = 0, genderCoefficient = 0;
var fetCoefficient = 0;
var G14 = 0;
var G6 = 0;
var A6 = 0;
var A4 = 0;
var B6 = 0
var C6 = 0;
var D6 = 0;

var genderCoefficientArray = new Array(2)
genderCoefficientArray[0]=1
genderCoefficientArray[1]=0.9;

var weekSpeedArray = new Array(2)
weekSpeedArray[0]=0.005
weekSpeedArray[1]=-0.01;

var fatBenchmarkArray = new Array(2)
fatBenchmarkArray[0]=1
fatBenchmarkArray[1]=0.7;

var calorieArray = new Array(2)
calorieArray[0]=5500
calorieArray[1]=7700;

var proteinBenchmarkArray = new Array(2)
proteinBenchmarkArray[0]=2.5
proteinBenchmarkArray[1]=2;

var fetCoefficientArray = new Array(2)
fetCoefficientArray[0]=1
fetCoefficientArray[1]=0.7;

var advisez ='<img src="./碳蛋脂摄入量计算_files/zj_01.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_02.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_03.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_04.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_05.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_06.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/zj_07.jpg">';
var advisej ='<img src="./碳蛋脂摄入量计算_files/jz_01.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_02.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_03.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_04.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_05.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_06.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/jz_07.jpg">';
var advise ='<img src="./碳蛋脂摄入量计算_files/bc_01.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_02.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_03.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_04.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_05.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_06.jpg">'+
        '<img src="./碳蛋脂摄入量计算_files/bc_07.jpg">';
$("#gender").change(function () {
    setGender();
    getbmr();
    getheat();
});
$("#fetCoefficient").change(function () {
    setFetCoefficient();
    getbmr();
    getheat();
});
$("#weight,#stature,#age").keyup(function () {
    getbmr();
    getheat();
});

$("#day2").keyup(function () {
    getbmr();
    getheat();
});

$("#gain").change(function () {
    setPrupose();
    setSpeed();
    getbmr();
    getheat();
});

$("#weekspeed").change(function () {
    setSpeed();
    getbmr();
    getheat();
});

$("#activity").change(function () {
    getbmr();
    getheat();
});

$("#gainkg").keyup(function () {
    getheat();
});



function setGender(){
    var g = $("#gender").val();
    if(g){
        if(g=="male"){
            $('#fetCoefficient option').removeAttr('selected').filter('[id=male1]').attr('selected', true);
            genderCoefficient = genderCoefficientArray[0];
            for(var i = 1;i<=4;i++){
                document.getElementById("male"+i).removeAttribute("disabled");
                document.getElementById("female"+i).setAttribute("disabled", "disabled");
            }
        }else{
            $('#fetCoefficient option').removeAttr('selected').filter('[id=female1]').attr('selected', true);
            genderCoefficient = genderCoefficientArray[1];
            for(var i = 1;i<=4;i++){
                document.getElementById("female"+i).removeAttribute("disabled");
                document.getElementById("male"+i).setAttribute("disabled", "disabled");
            }
        }
    }else{
        $('#fetCoefficient option').removeAttr('disabled')
    }
    
    console.log(genderCoefficient);
}

function setFetCoefficient(){
    var g = $("#fetCoefficient").val();
    fetCoefficient = g;
    console.log(fetCoefficient);
}

function setPrupose(){
    var gain = $("#gain").val() - 1;
    //weekSpeed = weekSpeedArray[gain];
    fatBenchmark = fatBenchmarkArray[gain];
    calorie = calorieArray[gain];
    proteinBenchmark = proteinBenchmarkArray[gain];


    if($("#gain").val()){
        if($("#gain").val()==1){
            $('#weekspeed option').removeAttr('selected').filter('[id=increasespeed1]').attr('selected', true);
            genderCoefficient = genderCoefficientArray[0];
            for(var i = 1;i<=3;i++){
                document.getElementById("increasespeed"+i).removeAttribute("disabled");
            }
            for(var i = 1;i<=4;i++){
                document.getElementById("decreasespeed"+i).setAttribute("disabled", "disabled");
            }
        }else{
            $('#weekspeed option').removeAttr('selected').filter('[id=decreasespeed1]').attr('selected', true);
            genderCoefficient = genderCoefficientArray[1];
            for(var i = 1;i<=4;i++){
                document.getElementById("decreasespeed"+i).removeAttribute("disabled");
            }
            for(var i = 1;i<=3;i++){
                document.getElementById("increasespeed"+i).setAttribute("disabled", "disabled");
            }
        }
    }else{
        $('#weekspeed option').removeAttr('disabled')
    }
}

function setSpeed(){
    var g = $("#weekspeed").val();
    weekSpeed = g;
    console.log(weekSpeed);
}

function getbmr(){
    var w = $("#weight").val();
    var h = $("#stature").val();
    var a = $("#age").val();
    var g = $("#gender").val();
    bmr = w * genderCoefficient * fetCoefficient * 24;
    $("#bmr").text(Math.round(bmr));
}
function getheat(){
    var g = $("#gender").val();
    var act = $("#activity").val();
    var gkg = $("#gainkg").val();
    var w = $("#weight").val();
    var h = $("#stature").val();
    var a = $("#age").val();
    var gain = $("#gain").val();
    A4 = (w * weekSpeed) * 30 / 7;
    G6 = (A4 * calorie) / 30;
    activebmr = act * bmr;
    console.log(activebmr);
    A6 = (bmr + activebmr + G6) / 0.9;
    B6 = w * proteinBenchmark;
    C6 = w * fatBenchmark;
    if(A6 > bmr) {
        D6 = (A6 - B6*4 - C6*9)/4
    } else {
        D6 = (bmr - B6*4 - C6*9)/4
    }

    if(A6 < bmr) {
        $("#libmr").addClass("cbbnt");
        $("#liheat").removeClass("cbbnt");
    }

    if(A6 > bmr) {
        $("#libmr").removeClass("cbbnt");
        $("#liheat").addClass("cbbnt");
    }
    $("#poid").text(Math.round(A4*1000)/1000);
    $("#heat").text(Math.round(A6*10)/10);
    $("#num1").text(Math.round(B6));
    $("#num2").text(Math.round(C6));
    $("#num3").text(Math.round(D6*100)/100);
}

$( document ).ready(function() {
    setGender();
    setPrupose();
    setSpeed();
    setFetCoefficient();
    getbmr();
    getheat();
});